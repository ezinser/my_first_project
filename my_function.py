{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e72f7736-ef79-400d-8ca6-eb11a890a44e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "62c327ed-429e-412d-a60c-17a37e77008a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def myfunc(a):\n",
    "    return a * 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "74b06c38-965a-4060-a40f-5bcc5925ed58",
   "metadata": {},
   "outputs": [],
   "source": [
    "def addone(a):\n",
    "    return a + 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "0cb02e07-86be-48af-ae1f-6c203cff3671",
   "metadata": {},
   "outputs": [],
   "source": [
    "def addtwo(a):\n",
    "    return a + 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5d02d04-ff7c-4966-be48-c86bb28cb082",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
